package visitors;

import lrs.LRStruct;

public class ToStringVisitor<E> implements lrs.LRStruct.IAlgo<String, E, Void> {

	@Override
	public String emptyCase(LRStruct<E> host, Void arg) {
		return "\n DONE!";
	}

	@Override
	public String nonEmptyCase(LRStruct<E> host, Void arg) {
		return "\n" + host.getDatum().toString() + host.getRest().execute(this, null);
	}

	
}
