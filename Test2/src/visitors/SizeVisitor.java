package visitors;

import lrs.LRStruct;
import lrs.LRStruct.IAlgo;

public class SizeVisitor<E> implements LRStruct.IAlgo<Integer, E, Void> {

	@Override
	public Integer emptyCase(LRStruct<E> host, Void __) {return 0; }

	@Override
	public Integer nonEmptyCase(LRStruct<E> host, Void __) { return 1 + host.getRest().execute(this, __); }

}

