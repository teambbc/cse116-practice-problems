package visitors;

import lrs.LRStruct;
import lrs.LRStruct.IAlgo;

public class ContainsVisitor<E> implements LRStruct.IAlgo<Boolean, E, E> {

	@Override
	public Boolean emptyCase(LRStruct<E> host, E arg) {
		return false;
	}

	@Override
	public Boolean nonEmptyCase(LRStruct<E> host, E arg) {
		return host.getDatum().equals(arg) || host.getRest().execute(this, arg);
	}

}
