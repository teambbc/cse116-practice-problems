package visitors;

import lrs.LRStruct;

public class CopyVisitor<E> implements LRStruct.IAlgo<lrs.LRStruct<E>, E, Void> {

	@Override
	public LRStruct<E> emptyCase(LRStruct<E> host, Void arg) {
		return host;
	}

	@Override
	public LRStruct<E> nonEmptyCase(LRStruct<E> host, Void arg) {
		return host.getRest().execute(this, arg).insertFront(host.getDatum());	
	}

}
