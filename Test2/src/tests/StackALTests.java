package tests;

import org.junit.Test;

import stack.StackAL;

import static org.junit.Assert.assertTrue;

public class StackALTests {
	public void lifoPeak(String [] s) {
		StackAL<String> stack =  new StackAL<String>();
		String expected = s[s.length-1];
		for(int i = 0; i < s.length; i++) {
			stack.push(s[i]);
		}
		String actual = stack.peek();
		assertTrue("", expected.equals(actual));
	}
	
	@Test public void lifoPeak0() {
		String [] s = {"David", "Scott"};
		this.lifoPeak(s);
	}
	@Test public void lifoPeak1() {
		String [] s = {"David"};
		this.lifoPeak(s);
	}
	@Test public void lifoPeak2() {
		String [] s = {"David"};
		this.lifoPeak(s);
	}
	
	public void lifoPop(String [] s) {
		StackAL<String> stack =  new StackAL<String>();
		String expected = s[s.length-1];
		for(int i = 0; i < s.length; i++) {
			stack.push(s[i]);
		}
		String actual = stack.pop();
		assertTrue("", expected.equals(actual));
	}
	
	@Test public void life0() {
		String [] s = {"David", "Scott"};
		this.lifoPop(s);
	}
	@Test public void life1() {
		String [] s = {"David"};
		this.lifoPop(s);
	}
	@Test public void life2() {
		String [] s = {"David"};
		this.lifoPop(s);
	}
	
	public void lifoSize(String [] s) {
		StackAL<String> stack =  new StackAL<String>();
		int expected = s.length;
		for(int i = 0; i < s.length; i++) {
			stack.push(s[i]);
		}
		int actual = stack.size();
		assertTrue("expected: " + s.length+ " got: " + actual, expected == actual);
	}
	
	@Test public void lifoSize0() {
		String [] s = {"David", "Scott"};
		this.lifoSize(s);
	}
	@Test public void lifoSize1() {
		String [] s = {"David"};
		this.lifoSize(s);
	}
	@Test public void lifoSize2() {
		String [] s = {"David"};
		this.lifoSize(s);
	}
}
