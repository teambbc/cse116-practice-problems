//package tests;
//
//import org.junit.Test;
//
//import queue.QueueAL;
//
//import static org.junit.Assert.assertTrue;
//
//public class QueueALTests {
//	public void fifoPeak(String [] s) {
//		QueueAL<String> stack =  new QueueAL<String>();
//		String expected = s[0];
//		for(int i = 0; i < s.length; i++) {
//			stack.enqueue(s[i]);
//		}
//		String actual = stack.peakAtNext();
//		assertTrue("", expected.equals(actual));
//	}
//	
//	@Test public void fifoPeak0() {
//		String [] s = {"David", "Scott"};
//		this.fifoPeak(s);
//	}
//	@Test public void fifoPeak1() {
//		String [] s = {"David"};
//		this.fifoPeak(s);
//	}
//	@Test public void fifoPeak2() {
//		String [] s = {"David"};
//		this.fifoPeak(s);
//	}
//	
//	public void fifoDequeue(String [] s) {
//		QueueAL<String> stack =  new QueueAL<String>();
//		String expected = s[0];
//		for(int i = 0; i < s.length; i++) {
//			stack.enqueue(s[i]);
//		}
//		String actual = stack.dequeue();
//		assertTrue("", expected.equals(actual));
//	}
//	
//	@Test public void fifoDeq0() {
//		String [] s = {"David", "Scott"};
//		this.fifoDequeue(s);
//	}
//	@Test public void fifoDeq1() {
//		String [] s = {"David"};
//		this.fifoDequeue(s);
//	}
//	@Test public void fifoDeq2() {
//		String [] s = {"David"};
//		this.fifoDequeue(s);
//	}
//	
//	public void fifoSize(String [] s) {
//		QueueAL<String> stack =  new QueueAL<String>();
//		int expected = s.length;
//		for(int i = 0; i < s.length; i++) {
//			stack.enqueue(s[i]);
//		}
//		int actual = stack.size();
//		assertTrue("expected: " + s.length+ " got: " + actual, expected == actual);
//	}
//	
//	@Test public void lifoSize0() {
//		String [] s = {"David", "Scott"};
//		this.fifoSize(s);
//	}
//	@Test public void lifoSize1() {
//		String [] s = {"David"};
//		this.fifoSize(s);
//	}
//	@Test public void lifoSize2() {
//		String [] s = {"David"};
//		this.fifoSize(s);
//	}
//}
