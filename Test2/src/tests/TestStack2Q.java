package tests;

import org.junit.Test;

import stack.StackWithTwoQs;

import static org.junit.Assert.assertTrue;
public class TestStack2Q {
	public void boilerPop(Integer [] a) {
		StackWithTwoQs<Integer> s = new StackWithTwoQs<Integer>();
		for (int i = 0; i < a.length; i++) {
			s.push(a[i]);
		}

		int expected = a[a.length-1];
		int actual = s.pop();
		assertTrue("Expected : " + expected + " got;  " + actual, expected == actual);	
	}
	
	@Test public void pop0 () {
		Integer[] a = {1};
		this.boilerPop(a);
	}
	@Test public void pop1 () {
		Integer[] a = {1,2};
		this.boilerPop(a);
	}
	@Test public void pop2 () {
		Integer[] a = {1,2,3};
		this.boilerPop(a);
	}
}
