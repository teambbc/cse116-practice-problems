//package tests;
//
//import org.junit.Test;
//
//import queue.QueueLRS;
//
//import static org.junit.Assert.assertTrue;
//
//public class QueueLRSTests {
//	public void fifoPeak(String [] s) {
//		QueueLRS<String> stack =  new QueueLRS<String>();
//		String expected = s[0];
//		for(int i = 0; i < s.length; i++) {
//			stack.enqueue(s[i]);
//		}
//		System.out.println(stack.toString());
//		String actual = stack.peakAtNext();
//		assertTrue("", expected.equals(actual));
//	}
//	
//	@Test public void fifoPeak0() {
//		String [] s = {"David", "Scott"};
//		this.fifoPeak(s);
//	}
//	@Test public void fifoPeak1() {
//		String [] s = {"David"};
//		this.fifoPeak(s);
//	}
//	@Test public void fifoPeak2() {
//		String [] s = {"David", "Scott", "Blair"};
//		this.fifoPeak(s);
//	}
//	
//	public void fifoDequeue(String [] s) {
//		QueueLRS<String> stack =  new QueueLRS<String>();
//		String expected = s[0];
//		for(int i = 0; i < s.length; i++) {
//			stack.enqueue(s[i]);
//		}
//		String actual = stack.dequeue();
//		assertTrue("", expected.equals(actual));
//	}
//	
//	@Test public void fifoDeq0() {
//		String [] s = {"David", "Scott"};
//		this.fifoDequeue(s);
//	}
//	@Test public void fifoDeq1() {
//		String [] s = {"David"};
//		this.fifoDequeue(s);
//	}
//	@Test public void fifoDeq2() {
//		String [] s = {"David"};
//		this.fifoDequeue(s);
//	}
//	
//	public void fifoSize(String [] s) {
//		QueueLRS<String> stack =  new QueueLRS<String>();
//		int expected = s.length;
//		for(int i = 0; i < s.length; i++) {
//			stack.enqueue(s[i]);
//		}
//		int actual = stack.size();
//		assertTrue("expected: " + s.length+ " got: " + actual, expected == actual);
//	}
//	
//	@Test public void lifoSize0() {
//		String [] s = {"David", "Scott"};
//		this.fifoSize(s);
//	}
//	@Test public void lifoSize1() {
//		String [] s = {"David"};
//		this.fifoSize(s);
//	}
//	@Test public void lifoSize2() {
//		String [] s = {"David"};
//		this.fifoSize(s);
//	}
//	
//	public void isHeadEmpty(String [] s) {
//		QueueLRS<String> q = new QueueLRS<String>();
//		boolean expected = true;
//		for(int i = 0; i < s.length; i++) {
//			q.enqueue(s[i]);
//		}
//		boolean actual = q.isTailEmpty();
//		assertTrue("expected: " + true + " got: " + actual, expected == actual);
//	}
//	
//	@Test public void emptyHead0() {
//		String [] s = {"David"};
//		this.isHeadEmpty(s);
//	}
//	@Test public void emptyHead1() {
//		String [] s = {"David", "Scott"};
//		this.isHeadEmpty(s);
//	}
//	@Test public void emptyHead2() {
//		String [] s = {"David", "Scott", "Blair"};
//		this.isHeadEmpty(s);
//	}
//}
