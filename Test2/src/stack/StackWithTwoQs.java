package stack;

public class StackWithTwoQs<E> implements Stack<E>{
	private queue.QueueLRS<E> _newQ;
	private queue.QueueLRS<E> _restQ;
	public StackWithTwoQs() {
		_newQ = new queue.QueueLRS<E>();
		_restQ = new queue.QueueLRS<E>();
	}
	@Override
	public void push(E item) {
		if(_newQ.isEmpty()) {
			_newQ.enqueue(item);
		}
		else {
			_restQ.enqueue(_newQ.dequeue());
			_newQ.enqueue(item);
		}
	}

	@Override
	public E pop() {
		E answer = _newQ.dequeue();
		E temp = null;
		
		while(!_restQ.isEmpty()) {
			temp = _restQ.dequeue();
			if(_restQ.isEmpty()) {break;}
			_newQ.enqueue(temp);
		}
		queue.QueueLRS<E> tempQ;
		tempQ = _newQ;
		_newQ = _restQ;
		_restQ = tempQ;
		return answer;
	}

	@Override
	public E peek() {
		return _newQ.peek();
	}

	@Override
	public boolean isEmpty() {
		return _newQ.isEmpty();
	}

}
