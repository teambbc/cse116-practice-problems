package stack;

import java.util.EmptyStackException;

import lrs.LRStruct;
import visitors.SizeVisitor;

public class StackLRS<E> implements Stack<E> {
	private LRStruct<E> _lrs;
	public StackLRS() {
		_lrs = new LRStruct<E>();
	}

	@Override public void push(E item) {_lrs.insertFront(item);}

	@Override public E pop() { 
		try {
			E result = _lrs.getDatum();
			_lrs.removeFront();
			return result;
		}
		catch (IllegalStateException e) {
			throw new EmptyStackException();
		}
	}

	@Override public E peek() {return _lrs.getDatum();}


	public int size() {
		return _lrs.execute(new SizeVisitor<E>(), null);
	}
	public boolean isEmpty() {
		return this._lrs.execute(
				new lrs.LRStruct.IAlgo<Boolean, E, Void>() {
					@Override public Boolean emptyCase(LRStruct<E> host, Void __) {return true;}
					@Override public Boolean nonEmptyCase(LRStruct<E> host, Void __) {return false;}
				}
		,null);
	}

}
