package stack;

import java.util.ArrayList;
import java.util.EmptyStackException;

/**
 * Implementation of STACK ADT using arrayList
 * @author davidblair
 * Methods: push, pop, peak, size
 */
public class StackAL<E> implements Stack<E>{
	ArrayList<E> _store;
	public StackAL() {_store = new ArrayList<E>();}
	
	@Override public void push(E item) {_store.add(item);}
	
	@Override public E pop() {
		try {
			E result = _store.get(_store.size() -1);
			_store.remove(result);
			return result;
		}
		catch (ArrayIndexOutOfBoundsException e){throw new EmptyStackException();}
	}
	
	@Override public E peek() { return _store.get(_store.size() -1); }

	public int size() {
		return _store.size();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
}
