package queue;

import lrs.LRStruct;
import lrs.LRStruct.IAlgo;

public class QueueLRS<E> implements Queue<E> {
	private LRStruct<E> _head;
	private LRStruct<E> _tail;
	public QueueLRS() {
		_head = new LRStruct<E>();
		_tail = _head;

	}
	// enqueue a the back of line
	@Override public void enqueue(E item) {
			_tail.insertFront(item);
			_tail = _tail.getRest();
	}
	//dequeue at the front
	@Override public E dequeue() {
		return _head.removeFront();
	}

	@Override public E peek() {
		return _head.getDatum();
	}

	public int size() {
		return _head.execute(new visitors.SizeVisitor<E>(), null);
	}
	public boolean isTailEmpty() {
		return _tail.execute(new lrs.LRStruct.IAlgo<Boolean, E, Void>() {
			@Override public Boolean emptyCase(LRStruct<E> host, Void arg) {return true;}
			@Override public Boolean nonEmptyCase(LRStruct<E> host, Void arg) {return false;}
		}, null);
	}
	
	@Override public String toString() {
		return _head.execute(new visitors.ToStringVisitor<E>(), null);
	}

	@Override
	public boolean isEmpty() {
		return _head.execute(new IAlgo<Boolean, E, Void>() {
			@Override public Boolean emptyCase(LRStruct<E> host, Void arg) {return true;}
			@Override public Boolean nonEmptyCase(LRStruct<E> host, Void arg) {return false;}
		}, null);	
	}
	@Override
	public String name() {
		// TODO Auto-generated method stub
		return null;
	}

}
