package queue;

public interface Queue<E> {

	public void enqueue(E item);

	public E dequeue();

	public E peek();

	public boolean isEmpty();

	public String name();

}