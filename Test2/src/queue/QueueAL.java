package queue;

import java.util.ArrayList;

public class QueueAL<E> implements Queue<E> {
	ArrayList<E> _al;
	
	public QueueAL() {
		_al = new ArrayList<E>();
	}
	/**
	 * runtime is usually constant
	 * in pathological case it is exponential
	 * amortized linear
	 */
	@Override public void enqueue(E item) {
		_al.add(item);
	}
	/**
	 * runtime is linear
	 */
	@Override public E dequeue() {
		return _al.remove(0);
	}

	@Override public E peek() {
		return _al.get(0);
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public String name() {
		// TODO Auto-generated method stub
		return null;
	}


	
}
