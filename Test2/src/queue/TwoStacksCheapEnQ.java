package queue;

public class TwoStacksCheapEnQ<E> implements Queue<E>  {
	//push onto stack 0 pop from stack1
	private stack.StackLRS<E> _stack0;
	private stack.StackLRS<E> _stack1;
	public TwoStacksCheapEnQ() {
		_stack0 = new stack.StackLRS<>();
		_stack1 = new stack.StackLRS<>();
	}
	@Override public void enqueue(E item) {
		_stack0.push(item);
	}
	@Override public E dequeue() {
		if(_stack1.isEmpty()) { 
			while(!_stack0.isEmpty()) {_stack1.push(_stack0.pop());}
		}
		return _stack1.pop();
	}
	@Override
	public E peek() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public String name() {
		// TODO Auto-generated method stub
		return null;
	}

}
